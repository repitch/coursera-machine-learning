#Programming Assignment: Нормализация признаков

Link: https://www.coursera.org/learn/vvedenie-mashinnoe-obuchenie/programming/w7Rqc/normalizatsiia-priznakov

Данное задание основано на материалах лекции по линейным методам классификации.

##Вы научитесь:

работать с персептроном — простейшим вариантом линейного классификатора
повышать качество линейной модели путем нормализации признаков