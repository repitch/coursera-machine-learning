# -*- coding: utf-8 -*-
import pandas as pd
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from core.fileutils import write_res_txt

RANDOM_STATE = 241

def main():
    print "Main"
    train = pd.read_csv("perceptron-train.csv", header=None)
    train_y = train[0]
    train_x = train.loc[:, 1:]

    test = pd.read_csv("perceptron-test.csv", header=None)
    test_y = test[0]
    test_x = test.loc[:, 1:]

    model = Perceptron(random_state=RANDOM_STATE)
    model.fit(train_x, train_y)
    score1 = accuracy_score(test_y, model.predict(test_x))
    print "Score 1: %f" % score1

    scaler = StandardScaler()
    train_x_scaled = scaler.fit_transform(train_x)
    test_x_scaled = scaler.transform(test_x)

    model.fit(train_x_scaled, train_y)
    score2 = accuracy_score(test_y, model.predict(test_x_scaled))

    print "Score 2: %f" % score2

    score_diff = score2 - score1
    print "Score 2 better than 1 on %.3f" % score_diff
    write_res_txt(1, "%.3f" % score_diff)

main()
