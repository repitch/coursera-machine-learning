# -*- coding: utf-8 -*-
import pandas as pd
from sklearn.cross_validation import KFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import scale


def main():
    data = pd.read_csv('wine.data.csv', header=None)

    y = data[0]
    X = data.ix[:, 1:]

    print "Without scaling"
    best_score = test_KNN(X, y)
    print "Best score %.2f for %d neighbours" % (best_score['score'], best_score['n'])
    write_res_txt(1, best_score['n'])
    write_res_txt(2, "%.2f" % best_score['score'])

    print "Making scaling"
    X_scaled = pd.DataFrame(scale(X))
    best_score2 = test_KNN(X_scaled, y)
    print "Best score %.2f for %d neighbours" % (best_score2['score'], best_score2['n'])
    write_res_txt(3, best_score2['n'])
    write_res_txt(4, "%.2f" % best_score2['score'])


def write_res_txt(ind, result):
    text_file = open("submission/q%d.txt" % ind, "w")
    text_file.write("%s" % result)
    text_file.close()


def test_KNN(X, y):
    kf = KFold(len(X), n_folds=5, shuffle=True, random_state=42)
    i = 1
    best_score = {'score': 0, 'n': 0}
    while i <= 50:
        # print "%d neighbours" % i
        neigh = KNeighborsClassifier(n_neighbors=i, )
        k = 0
        mean_score = 0
        for train, test in kf:
            neigh.fit(X.iloc[train], y.iloc[train])
            score = neigh.score(X.iloc[test], y.iloc[test])
            mean_score += score
            k += 1
        mean_score /= k
        # print ("Mean score: %f" % mean_score)
        if mean_score > best_score['score']:
            best_score['score'] = mean_score
            best_score['n'] = i
        i += 1
    return best_score


main()
