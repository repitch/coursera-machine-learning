# -*- coding: utf-8 -*-
import numpy as np
from sklearn import datasets
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import scale


def main():
    boston = datasets.load_boston()
    X = scale(boston.data)
    y = boston.target

    best_score = {'score': 0, 'p': 0}
    kf = KFold(len(X), n_folds=5, shuffle=True, random_state=42)
    for p in np.linspace(1.0, 10.0, num=200):
        regressor = KNeighborsRegressor(n_neighbors=5,
                                        weights='distance',
                                        metric='minkowski',
                                        p=p)
        val_score = cross_val_score(regressor,
                                    X, y=y,
                                    scoring='mean_squared_error',
                                    cv=kf)
        mean_score = np.mean(val_score)
        if best_score['score'] == 0:
            best_score['score'] = mean_score
            best_score['p'] = p
        elif mean_score > best_score['score']:
            best_score['score'] = mean_score
            best_score['p'] = p
    print best_score

    res = best_score['p']
    if float(res).is_integer():
        write_res_txt(1, "%d" % res)
    else:
        write_res_txt(1, "%.1f" % res)


def write_res_txt(ind, result):
    text_file = open("submission/q%d.txt" % ind, "w")
    text_file.write("%s" % result)
    text_file.close()


main()
