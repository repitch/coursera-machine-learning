#Peer Graded Assignment: Проект: предсказания победителя в онлайн-игре

Link:
https://www.coursera.org/learn/vvedenie-mashinnoe-obuchenie/peer/J1SH8/proiekt-priedskazaniia-pobieditielia-v-onlain-ighrie

Данное задание основано на материалах лекций по линейной регрессии и посвящено предсказанию оклада, исходя из описания вакансии.

##Вы научитесь:

- использовать линейную регрессию
- применять линейную регрессию к текстовым данным