import datetime

import numpy as np
import pandas as pd
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.ensemble import GradientBoostingClassifier

result_fields = [
    'duration',
    'tower_status_radiant',
    'tower_status_dire',
    'barracks_status_radiant',
    'barracks_status_dire',
    'radiant_win'
]
target_field = 'radiant_win'


def variant1(X_train, y_train, X_test):
    '''
    Gradient boosting
    '''
    # task 2
    ft_len = len(X_train)
    ft_count = ft_len - X_train.count().sort_values()
    for index, row in ft_count.iteritems():
        if row == 0:
            # drop items without misses
            ft_count = ft_count.drop(index)
    print 'Total items: %d. Misses columns: %d' % (ft_len, len(ft_count))
    print 'Misses:'
    print ft_count
    # task 3
    X_train.fillna(0, inplace=True)
    # task 4
    print 'Target variable: %s' % target_field
    # task 5
    kf = KFold(ft_len, n_folds=5, shuffle=True, random_state=42)

    n_estimators = 30
    print "start cross validation, n_estimators=%d" % n_estimators
    start_time = datetime.datetime.now()
    clf = GradientBoostingClassifier(n_estimators=n_estimators, random_state=42)
    score = np.mean(cross_val_score(clf, X_train, y_train, scoring='roc_auc', cv=kf))
    print "finish cross validation, score=%f, time: %s" % (score, datetime.datetime.now() - start_time)


def main():
    features = pd.read_csv('features.csv', index_col='match_id')
    features_test = pd.read_csv('features_test.csv', index_col='match_id')

    X_train = features.drop(result_fields, axis=1)
    y_train = features[target_field]
    variant1(X_train, y_train, features_test)



    # clf =..
    # clf.predict_proba(features_text)[:, 1]


main()
