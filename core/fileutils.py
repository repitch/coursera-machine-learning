# -*- coding: utf-8 -*-
import os
SUBMISSION = "submission"


def write_res_txt(ind, result):
    if not os.path.exists(SUBMISSION):
        os.makedirs(SUBMISSION)
    text_file = open("%s/q%d.txt" % (SUBMISSION, ind), "w")
    text_file.write("%s" % result)
    text_file.close()
