# -*- coding: utf-8 -*-
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier

data = pd.read_csv('titanic.csv', index_col='PassengerId')

label = LabelEncoder()
dicts = {}
label.fit(data.Sex.drop_duplicates())  # задаем список значений для кодирования
dicts['Sex'] = list(label.classes_)
data.Sex = label.transform(data.Sex)  # заменяем значения из списка кодами закодированных элементов

# remove ages
data = data[data['Age'].notnull()]

X = data[['Pclass', 'Fare', 'Age', 'Sex']]
Y = data['Survived']

clf = DecisionTreeClassifier(random_state=241)
clf.fit(X, Y)

importance = clf.feature_importances_
print(importance)
