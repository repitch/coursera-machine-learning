import pandas

data = pandas.read_csv('titanic.csv', index_col='PassengerId')

# 1
males_mask = data['Sex'] == 'male'
females_mask = data['Sex'] == 'female'

print "%d %d" % (len(data[males_mask]), len(data[females_mask]))

# 2
survivors = data[data['Survived'] == 1]
print "%.2f" % (float(len(survivors)*100) / len(data))

# 3 class
first_class = data[data['Pclass'] == 1]
print "%.2f" % (float(len(first_class)*100) / len(data))

# 4 mean age
print "%f %f" % (data['Age'].mean(), data['Age'].median())

# 5 correlation
print data.corr(method='pearson')

# famous name
females = data[females_mask]
mrs = females[females['Name'].str.contains('Mrs.')]
miss = females[females['Name'].str.contains('Miss.')]
mrs_counts = mrs['Name'].str.split('(').str[1].str.split(' ').str[0].value_counts()
print mrs_counts.head(5)
miss_counts = miss['Name'].str.split(' ').str[2].value_counts()
print miss_counts.head(5)
# print len(mrs)
# print len(miss)
# names_count = females['Name'].str.split('(').str[1].str.split(' ').str[0].value_counts()
# print names_count.head()
