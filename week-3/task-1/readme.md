#Programming Assignment: Опорные объекты

Link: https://www.coursera.org/learn/vvedenie-mashinnoe-obuchenie/programming/50VrR/opornyie-obiekty

Данное задание основано на материалах лекций по методам опорных векторов.

##Вы научитесь:

- работать с методом опорных векторов (SVM)
- находить наиболее важные объекты выборки