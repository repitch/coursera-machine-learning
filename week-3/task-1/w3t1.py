# -*- coding: utf-8 -*-
from core.fileutils import write_res_txt
from sklearn.svm import SVC
import pandas as pd


def build_out_string(indexes):
    if len(indexes) == 0:
        return None
    string = ""
    for ind in indexes:
        string += "%d " % (ind + 1)  # plus one because numeration starts with 0
    return string[:-1]


def main():
    data = pd.read_csv('svm-data.csv', header=None)
    X = data.loc[:, 1:]
    y = data[0]

    classifier = SVC(C=100000, kernel='linear', random_state=241)
    classifier.fit(X, y)
    out_str = build_out_string(classifier.support_)
    print out_str
    write_res_txt(1, out_str)


main()
