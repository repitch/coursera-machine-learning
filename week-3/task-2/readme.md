#Programming Assignment: Анализ текстов

Link: https://www.coursera.org/learn/vvedenie-mashinnoe-obuchenie/programming/NdyLO/analiz-tiekstov

Данное задание основано на материалах лекций по методу опорных векторов.

##Вы научитесь:

- находить оптимальные параметры для метода опорных векторов
- работать с текстовыми данными