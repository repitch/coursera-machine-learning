# -*- coding: utf-8 -*-
from core.fileutils import write_res_txt
from sklearn.svm import SVC
import pandas as pd
import numpy as np
from sklearn import datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import KFold, cross_val_score
import time


def main():
    start_time = time.time()
    ng = datasets.fetch_20newsgroups(subset='all',
                                     categories=['alt.atheism', 'sci.space'])
    x = ng.data
    y = ng.target

    vectorizer = TfidfVectorizer()
    vectorizer.fit_transform(x)

    test_x = vectorizer.transform(x)

    cv = KFold(y.size, n_folds=5, shuffle=True, random_state=241)
    grid = {'C': np.power(10.0, np.arange(-5, 6))}
    clf = SVC(kernel='linear', random_state=241)
    gs = GridSearchCV(clf, grid, scoring='accuracy', cv=cv)
    gs.fit(test_x, y)

    score = 0
    C = 0
    for suggest in gs.grid_scores_:
        if suggest.mean_validation_score > score:
            score = suggest.mean_validation_score
    C = suggest.parameters['C']

    model = SVC(kernel='linear', random_state=241, C=C)
    model.fit(test_x, y)

    words = vectorizer.get_feature_names()
    coef = pd.DataFrame(model.coef_.data, model.coef_.indices)
    top_words = coef[0].map(lambda w: abs(w)).sort_values(ascending=False).head(10).index.map(lambda i: words[i])
    top_words.sort()
    output = ','.join(top_words)
    print(output)
    write_res_txt(1, output)
    end_time = time.time() - start_time
    print "End time: %f" % end_time


main()
