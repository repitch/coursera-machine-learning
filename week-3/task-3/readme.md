#Programming Assignment: Логистическая регрессия

link: https://www.coursera.org/learn/vvedenie-mashinnoe-obuchenie/programming/MptFX/loghistichieskaia-rieghriessiia

Данное задание основано на материалах лекций по логистической регрессии.

##Вы научитесь:

работать с логистической регрессией
реализовывать градиентный спуск для ее настройки
использовать регуляризацию
