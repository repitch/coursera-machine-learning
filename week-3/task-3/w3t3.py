# -*- coding: utf-8 -*-
from core.fileutils import write_res_txt
from sklearn.svm import SVC
import pandas as pd
import time, math
from core.fileutils import write_res_txt
from sklearn.metrics import roc_auc_score

cC = 0
step = 0.1
w_start = [0, 0]
eps = 1e-5
MAX_ITER = 1e+4


def delta(w1new, w2new, w1, w2):
    dlt = math.sqrt((w1new - w1) ** 2 + (w2new - w2) ** 2)
    # print "delta %f" % dlt
    return dlt


def count_w1(w1, w2, x, y, c=0.0, k=0.1):
    sum = 0
    l = len(y)
    for i in range(0, l):
        sum += x[1][i] * y[i] * (1 - 1 / (1 + math.exp(-y[i] * (w1 * x[1][i] + w2 * x[2][i]))))
    return w1 + k / l * sum - k * c * w1


def count_w2(w1, w2, x, y, c=0.0, k=0.1):
    sum = 0
    l = len(y)
    for i in range(0, l):
        sum += x[2][i] * y[i] * (1 - 1 / (1 + math.exp(-y[i] * (w1 * x[1][i] + w2 * x[2][i]))))
    return w2 + k / l * sum - k * c * w2


def grad(y, x, c=0.0, w1=0.0, w2=0.0, k=0.1, err=1e-5):
    for i in range(0, 10000):
        w1new, w2new = count_w1(w1, w2, x, y, c, k), count_w2(w1, w2, x, y, c, k)
        dlt = delta(w1new, w2new, w1, w2)
        if dlt <= err:
            break
        else:
            w1, w2 = w1new, w2new
    return [w1new, w2new]


def a(x, w):
    #  a(x) = 1 / (1 + exp(-w1 x1 - w2 x2)).
    return 1 / (1 + math.exp(-w[0] * x[1] - w[1] * x[2]))


def main():
    global MAX_ITER, eps, w_start
    start_time = time.time()

    data = pd.read_csv('data-logistic.csv', header=None)
    x = data.loc[:, 1:]  # .as_matrix()
    y = data[0]

    w_arr = grad(y, x)
    w_arr_l2 = grad(y, x, 10.0)
    print 'w_arr    %s' % w_arr
    print 'w_arr_l2 %s' % w_arr_l2

    score = x.apply(lambda xi: a(xi, w_arr), axis=1)
    score_l2 = x.apply(lambda xi: a(xi, w_arr_l2), axis=1)

    auc = roc_auc_score(y, score)
    auc_l2 = roc_auc_score(y, score_l2)
    out_str = "%.3f %.3f" % (auc, auc_l2)
    write_res_txt(1, out_str)
    print out_str


main()
